from flask import Flask, request, jsonify, render_template
import pandas as pd
from typing import List, Dict, Union

app = Flask(__name__)

hw_01 = pd.read_csv('python-2024-hw-01-git.csv', encoding = 'utf-8')
hw_02 = pd.read_csv('python-2024-hw-02-типыиструктурыданных.csv', encoding = 'utf-8')
main_hw_01 = pd.read_csv('python-2024-main-hw-01.csv', encoding = 'utf-8')
main_hw_02 = pd.read_csv('python-2024-main-hw-02.csv', encoding = 'utf-8')
data: Dict[str, pd.DataFrame] = {
    'hw-01': hw_01,
    'hw-02': hw_02,
    'main-hw-01': main_hw_01,
    'main-hw-02': main_hw_02
}

@app.route('/names')
def get_names():
    names: List[str] = pd.concat([data[hw]['ФИ'] for hw in data]).unique().tolist()
    return jsonify({'names': names})

@app.route('/<hw_name>/mean_score/')
def get_hw_mean_score(hw_name):
    if hw_name not in data:
        return {'status': 'error', 'message': 'Такого дз нет ┐(￣ヘ￣;)┌'}, 404
    mean_score: float = data[hw_name]['Баллы'].mean()
    return jsonify({'mean_score': mean_score})

@app.route('/<hw_name>/<group_id>/mean_score/')
def get_hw_group_mean_score(hw_name, group_id):
    if hw_name not in data:
        return {'status': 'error', 'message': 'Такого дз нет ┐(￣ヘ￣;)┌'}, 404
    group_data: pd.DataFrame = data[hw_name][data[hw_name]['Группа'] == int(group_id)]
    mean_score: float = group_data['Баллы'].mean()
    return jsonify({'mean_score': mean_score})

@app.route('/mean_score/')
def get_mean_score():
    group_id: Union[str, None] = request.args.get('group_id')
    hw_name: Union[str, None] = request.args.get('hw_name')
    if hw_name not in data or group_id is None:
        return {'status': 'error', 'message': 'У Вас неправильные параметры ( ; ω ; )'}, 400
    group_data: pd.DataFrame = data[hw_name][data[hw_name]['Группа'] == int(group_id)]
    mean_score: float = group_data['Баллы'].mean()
    return jsonify({'mean_score': mean_score})

def get_grade(score: float) -> int:
    if score < 1:
        return 2
    elif score <= 30:
        return 3
    elif score <= 50:
        return 4
    else:
        return 5
        
@app.route('/mark/')
def get_mark():
    student_id: Union[str, None] = request.args.get('student_id')
    group_id: Union[str, None] = request.args.get('group_id')
    if student_id:
        student_data: pd.DataFrame = pd.concat([hw_01, hw_02]).loc[pd.concat([hw_01, hw_02])['student_id'] == int(student_id)]
        if student_data.empty:
            return {'status': 'error', 'message': 'Данных нет ( ╥ω╥ )'}, 404
        total_score: float = student_data['Баллы'].sum()
        mark: int = get_grade(total_score)
        return jsonify({'mark': mark})
    if group_id:
        group_data: pd.DataFrame = pd.concat([hw_01, hw_02]).loc[pd.concat([hw_01, hw_02])['Группа'] == int(group_id)]
        if group_data.empty:
            return {'status': 'error', 'message': 'Данных нет ( ╥ω╥ )'}, 404
        mean_score: float = group_data['Баллы'].mean()
        mark: int = get_grade(mean_score)
        return jsonify({'mean_mark': mark})
    return {'status': 'error', 'message': 'У Вас неправильные параметры ( ; ω ; )'}, 400

@app.route('/course_table/')
def render_course_table():
    hw_name: Union[str, None] = request.args.get('hw_name')
    group_id: Union[str, None] = request.args.get('group_id')
    if not hw_name or hw_name not in data:
        return {'status': 'error', 'message': 'Такого дз нет ┐(￣ヘ￣;)┌'}, 404
    if group_id:
        df: pd.DataFrame = data[hw_name][data[hw_name]['Группа'] == int(group_id)]
    else:
        df: pd.DataFrame = data[hw_name]
    if df.empty:
        return {'status': 'error', 'message': 'Данных нет ( ╥ω╥ )'}, 404
    return render_template('table.html', data=df.to_dict(orient='records'), columns=df.columns.tolist())

app.run(host='0.0.0.0', port=1337)